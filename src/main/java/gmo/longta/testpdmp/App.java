package gmo.longta.testpdmp;

import org.apache.commons.lang3.StringUtils;

/**
 * Hello world!
 * mvn compile exec:java -Dexec.mainClass="gmo.longta.testpdmp.App"
 */
public class App 
{
	public class RuningType{
		public static final String SYSTEM_TYPE      = "SYSTEM";
		public static final String ADMIN_DB_NAME_SY = "SYpdmp_admin";
		public static final String ADMIN_DB_USER_SY = "SYgmo_pdmp";
		public static final String ADMIN_DB_PASS_SY = "SYgmo_pdmp";
		
		public static final String UNIT_TEST_TYPE   = "UNIT_TEST";
		public static final String ADMIN_DB_NAME_UT = "UTpdmp_admin";
		public static final String ADMIN_DB_USER_UT = "UTgmo_pdmp";
		public static final String ADMIN_DB_PASS_UT = "UTgmo_pdmp";
		
		public static final String STORY_TEST_TYPE  = "STORY_TEST";
		public static final String ADMIN_DB_NAME_ST = "STpdmp_admin";
		public static final String ADMIN_DB_USER_ST = "STgmo_pdmp";
		public static final String ADMIN_DB_PASS_ST = "STgmo_pdmp";
		
		
	}
	
	public static String ADMIN_DB_HOST = "localhost";
	public static String ADMIN_DB_NAME = "";
	public static String ADMIN_DB_USER = "";
	public static String ADMIN_DB_PASS = "";
	public static String LONGTA = "longta";
	static {
		String host = System.getenv().get("ADMIN_DB_HOST");
		if (!StringUtils.isEmpty(host)) {
			if(host.equals(RuningType.UNIT_TEST_TYPE)){
				ADMIN_DB_NAME = RuningType.ADMIN_DB_NAME_UT;
				ADMIN_DB_USER = RuningType.ADMIN_DB_USER_UT;
				ADMIN_DB_PASS = RuningType.ADMIN_DB_PASS_UT;
			}else if(host.equals(RuningType.STORY_TEST_TYPE)){
				ADMIN_DB_NAME = RuningType.ADMIN_DB_NAME_ST;
				ADMIN_DB_USER = RuningType.ADMIN_DB_USER_ST;
				ADMIN_DB_PASS = RuningType.ADMIN_DB_PASS_ST;
			}else{
				ADMIN_DB_NAME = RuningType.ADMIN_DB_NAME_SY;
				ADMIN_DB_USER = RuningType.ADMIN_DB_USER_SY;
				ADMIN_DB_PASS = RuningType.ADMIN_DB_PASS_SY;
			}
		}
	}
	
    public static void main( String[] args )
    {
        System.out.println( "Hello World! : " + System.getenv("MARIA_DB_NAME") );
        System.out.println( "DB Name : " + ADMIN_DB_NAME);
        System.out.println( "User    : " + ADMIN_DB_USER);
        System.out.println( "Pass    : " + ADMIN_DB_PASS);
        		
        
    }
}
